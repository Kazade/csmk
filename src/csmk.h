#pragma once

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef float seconds_t;

enum smk_result {
    SMK_OK = 0,
    SMK_INVALID_ENUM = -1,
    SMK_INVALID_VALUE = -2,
    SMK_INVALID_OPERATION = -3,
    SMK_OUT_OF_MEMORY = -4
};

typedef struct smk_palette_t {
    uint8_t colors[256 * 3];
    int refcount;  // Number of frames referencing this palette
} smk_palette_t;

typedef struct smk_frame_t {
    uint32_t id;  // 0-based frame number
    uint8_t* data;  // Decoded frame data
    bool is_keyframe; // Whether or not this frame is a keyframe

    smk_palette_t* palette;  // Pointer to the possibly shared palette

    /* Linked list to previous and next frames */
    struct smk_frame_t* prev;
    struct smk_frame_t* next;
} smk_frame_t;

typedef struct smk_frame_info_t {
    uint32_t frame_size;
    bool is_keyframe;
} smk_frame_info_t;

typedef struct {
    uint32_t width;
    uint32_t height;

    FILE* stream;

    uint8_t* data;
    uint32_t data_size;

    uint32_t fps;

    seconds_t length;
    uint32_t frame_count;

    smk_frame_info_t* frame_infos;
} smk_video_t;

typedef struct {
    const smk_video_t* video;
    smk_frame_t* decoded_frames;
    uint32_t decoded_frame_count;
    seconds_t seek_position;
    uint32_t current_frame;
} smk_player_t;


#define SMK_DEFAULT_MAX_BUFFER_FRAMES 5

#define SMK_DEF extern

/** Load a video from the specified file path */
SMK_DEF smk_video_t* smk_load_video(const char* filename);

/** Load a video from the specified data */
SMK_DEF smk_video_t* smk_parse_video(const uint8_t* data, uint32_t length);

/** Stream the video without loading it fully into memory */
SMK_DEF smk_video_t* smk_stream_video(FILE* filein);

SMK_DEF uint32_t smk_video_width(smk_video_t* video);
SMK_DEF uint32_t smk_video_height(smk_video_t* video);
SMK_DEF uint32_t smk_video_fps(smk_video_t* video);

/** Destroy a video and its related data */
SMK_DEF smk_result smk_free_video(smk_video_t* video);

/** Create a new player instance */
SMK_DEF smk_player_t* smk_create_player(const smk_video_t* video);

/**
 * Decode frames from the current seek position using the specified timeslice.
 *
 * Returns the number of frames that have been decoded and are ready to play. */
SMK_DEF int smk_player_buffer_frames(smk_player_t* player, uint32_t timeslice_in_ms);

/* Set the maximum number of frames that should be decoded in the buffer.*/
SMK_DEF int smk_player_set_max_buffer_frames(smk_player_t* player, uint32_t frames);

SMK_DEF int smk_player_seek_frame(smk_player_t* player, int frame);
SMK_DEF int smk_player_seek_time(smk_player_t* player, int ms);

/* Returns true if the player has finished, false otherwise */
SMK_DEF bool smk_player_is_finished(smk_player_t* player);

/* Gets the currently visible frame at the current seek time */
SMK_DEF smk_frame_t* smk_player_get_frame(smk_player_t* player);

/** Move the seek time by the specified number of seconds.
 *  Returns: new seek position */
SMK_DEF float smk_player_update(smk_player_t* player, float dt);

SMK_DEF int smk_free_player(smk_player_t* player);

#define CSMK_IMPLEMENTATION
#ifdef CSMK_IMPLEMENTATION

typedef struct {
    char signature[4];
    int32_t width;
    int32_t height;
    int32_t frames;
    int32_t frame_rate;
    uint32_t flags;
    int32_t audio_size[7];
    int32_t trees_size;
    int32_t mmap_size;
    int32_t mclr_size;
    int32_t full_size;
    int32_t type_size;
    int32_t audio_rate[7];
    int32_t unused;
} smk_file_header_t;

#define _LOG_ERROR(msg) \
    strcpy(last_error, (msg));

static char last_error[128] = {0};

static void _smk_init_video(smk_video_t* video) {
    video->width = 0;
    video->height = 0;
    video->data = NULL;
    video->stream = NULL;
    video->data_size = 0;
}

smk_video_t* smk_stream_video(FILE* filein) {
    smk_video_t* video = (smk_video_t*) malloc(sizeof(smk_video_t));
    _smk_init_video(video);
    video->stream = filein;

    fseek(filein, 0, SEEK_SET);

    smk_file_header_t header;
    fread(&header, sizeof(smk_file_header_t), 1, filein);

    if(strncmp(header.signature, "SMK2", 4) != 0) {
        _LOG_ERROR("Invalid signature");
        return nullptr;
    }

    video->width = header.width;
    video->height = header.height;
    if(header.frame_rate == 0) {
        video->fps = 10;
    } else {
        video->fps = (
            (header.frame_rate > 0) ?
            (header.frame_rate / 1000) :
            (100000 / -header.frame_rate)
        );
    }

    video->length = float(header.frames) / float(video->fps);
    video->frame_count = header.frames;

    video->frame_infos = (smk_frame_info_t*) malloc(sizeof(smk_frame_info_t) * video->frame_count);

    for(int i = 0; i < video->frame_count; ++i) {
        uint32_t frame_size;
        fread(&frame_size, sizeof(uint32_t), 1, filein);
        video->frame_infos[i].is_keyframe = ((frame_size & 1) == 1);
        video->frame_infos[i].frame_size =
    }

    return video;
}

uint32_t smk_video_width(smk_video_t* video) {
    return video->width;
}

uint32_t smk_video_height(smk_video_t* video) {
    return video->height;
}

uint32_t smk_video_fps(smk_video_t* video) {
    return video->fps;
}

smk_result smk_free_video(smk_video_t* video) {
    if(!video) {
        return SMK_INVALID_VALUE;
    }

    if(video->frame_infos) {
        free(video->frame_infos);
        video->frame_infos = nullptr;
    }

    if(video->stream) {
        fclose(video->stream);
        video->stream = NULL;
    }

    if(video->data) {
        free(video->data);
        video->data = NULL;
    }

    free(video);

    return SMK_OK;
}

smk_player_t* smk_create_player(const smk_video_t* video) {
    smk_player_t* player = (smk_player_t*) malloc(sizeof(smk_player_t));
    player->video = video;
    player->decoded_frames = 0;
    player->decoded_frame_count = 0;
    player->seek_position = 0;
    return player;
}

int smk_player_buffer_frames(smk_player_t* player, uint32_t timeslice_in_ms) {
    return 0;
}

smk_frame_t* smk_player_get_frame(smk_player_t* player) {
    return nullptr;
}

bool smk_player_is_finished(smk_player_t* player) {
    if(!player) {
        _LOG_ERROR("Attempted to access null player");
        return false;
    }

    return player->current_frame == player->video->frame_count;
}

int smk_free_player(smk_player_t *player) {
    free(player);
    return SMK_OK;
}

float smk_player_update(smk_player_t* player, float dt) {
    /* FIXME: Need to deal with moving past the last buffered frame. Probably
     * by not updating the current frame until the new one has been decoded */
    player->seek_position += dt;
    player->seek_position = (
        (player->seek_position > player->video->length) ?
        player->video->length : player->seek_position
    );

    return player->seek_position;
}

#endif


#ifdef __cplusplus
}
#endif


