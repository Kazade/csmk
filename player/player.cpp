#define CSMK_IMPLEMENTATION
#include "csmk.h"

#include <sys/time.h>

uint64_t system_time_in_ms() {
    struct timeval start;
    gettimeofday(&start, NULL);
    return (start.tv_sec * 1000) + (start.tv_usec / 1000);
}

int main(int argc, char* argv[]) {
    FILE* file = fopen("data/mgm.dat", "rb");

    if(!file) {
        return 1;
    }

    smk_video_t* video = smk_stream_video(file);

    printf("Width: %d\n", smk_video_width(video));
    printf("Height: %d\n", smk_video_height(video));
    printf("FPS: %d\n", smk_video_fps(video));

    auto current_time = system_time_in_ms();

    smk_player_t* player = smk_create_player(video);

    // Give a small amount of time per frame to decode
    const float decode_time = 1.0f / 120.0f;

    while(!smk_player_is_finished(player)) {
        auto new_time = system_time_in_ms();
        auto frame_time = float(new_time - current_time) * 0.001f;
        current_time = new_time;

        int ready = smk_player_buffer_frames(player, decode_time);
        if(!ready) {
            continue;
        }

        smk_frame_t* frame = smk_player_get_frame(player);
        smk_player_update(player, frame_time);

        // glTexImage2D(..., frame->data, video->width, video->height);
    }

    smk_free_player(player);
    smk_free_video(video);

    return 0;
}
