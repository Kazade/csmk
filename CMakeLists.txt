project(csmk)

include_directories(src)

add_executable(
    smk-player
    ${CMAKE_SOURCE_DIR}/player/player.cpp
)

